# This Python file uses the following encoding: utf-8

# Copyright(C) 2014 Alsari <ibnomer2011@hotmail.com>

import os
import time
import threading
import winsound
import configobj

import globalVars
import NVDAObjects

from cStringIO import StringIO
from validate import Validator
from camlorn_audio import *
from gui import messageBox as msg
from logHandler import log

LOADED = False
conf = None

defaults = StringIO("""#Audio Themes add-on Configuration File.
	using = string(default="Default")

#End of configuration File.
""")

def themesdir():
	return os.path.join(globalVars.appArgs.configPath, 'Themes')

def currentThemePath():
	using = conf['using']
	return os.path.abspath(os.path.join(themesdir(), using))

def play(filename, looping = False):
	if looping:
		winsound.PlaySound(filename, winsound.SND_ASYNC|winsound.SND_LOOP|winsound.SND_NODEFAULT)
	else:
		winsound.PlaySound(filename, winsound.SND_ASYNC|winsound.SND_NODEFAULT)

def stop():
	winsound.PlaySound(None, winsound.SND_ASYNC)

def getLocation(obj, parrole = 14, chrole = 15):
	if obj.parent.role != parrole:
		return None
	if (obj.previous is None) or (obj.previous.role != chrole):
		return 'first.wav'
	elif (obj.next is None) or (obj.next.role != chrole):
		return 'last.wav'
	else:
		return None

def play3D(snd, obj):
	"""Plays 3D audio bassed on object location
	"""
	AUDIO_WIDTH = 10.0 # Width of the audio display.
	AUDIO_DEPTH = 5.0 # Distance of listener from display.
	role = obj.role
	# Get coordinate bounds of desktop.
	desktop = NVDAObjects.api.getDesktopObject()
	desktop_max_x = desktop.location[2]
	desktop_max_y = desktop.location[3]
	desktop_aspect = float(desktop_max_y) / float(desktop_max_x)
	# Get location of the object.
	if obj.location != None:
		# Object has a location. Get its center.
		obj_x = obj.location[0] + (obj.location[2] / 2.0)
		obj_y = obj.location[1] + (obj.location[3] / 2.0)
	else:
		# Objects without location are assumed in the center of the screen.
		obj_x = desktop_max_x / 2.0
		obj_y = desktop_max_y / 2.0
	# Scale object position to audio display.
	position_x = (obj_x / desktop_max_x) * (AUDIO_WIDTH * 2) - AUDIO_WIDTH
	position_y = (obj_y / desktop_max_y) * (desktop_aspect * AUDIO_WIDTH * 2) - (desktop_aspect * AUDIO_WIDTH)
	position_y *= -1
	snd.set_position(position_x, position_y, AUDIO_DEPTH * -1)
	snd.play()


def setupConfig():
	global defaults, conf
	confspec=configobj.ConfigObj(defaults, list_values=False, encoding="UTF-8")
	confspec.newlines="\r\n"
	conf = configobj.ConfigObj(infile = os.path.join(globalVars.appArgs.configPath, 'Themes.ini'), create_empty = True, configspec=confspec, stringify=True)
	validated=conf.validate(Validator(), copy=True)
	if validated:
		conf.write()


#some helper functions.

#TODO: Remove this function and find a beter alternative.
def initialize_camlorn_audio():
	"""A silly work around the camlorn_audio initialization problem. if we just know the exact cause, we should fix it rather than consuming resources."""
	global LOADED
	while not LOADED:
		try:
			init_camlorn_audio()
			LOADED = True
		except WindowsError:
			log.warn("faild to initialize camlorn_audio. Retrying...")
			time.sleep(0.1)

def createSoundObjects():
	d = currentThemePath()
	snds = {}
	for fileName in os.listdir(d):
		f = os.path.join(d ,fileName)
		if os.path.isfile(f) and f.endswith("wav"):
			try:
				snds[fileName] = Sound3D(f)
				snds[fileName].set_rolloff_factor(0)
			except camlorn_audio.UnsupportedChannelCountError:
				log.warn("unable to load file %s" %f)
	return snds
