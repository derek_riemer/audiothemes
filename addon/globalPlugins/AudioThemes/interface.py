# This Python file uses the following encoding: utf-8

# Copyright(C) 2014 Alsari <ibnomer2011@hotmail.com>

import os
import wx
import winKernel
import shutil
import tempfile
import zipfile

import gui
import core
import ui
import controlTypes
import utils

from camlorn_audio import Sound3D, UnsupportedChannelCountError, InternalFileError
from utils import *
from gui import messageBox as msg

import addonHandler
addonHandler.initTranslation()

NO_THEME_MSG = _("Turn Off Audio Themes")

class ThemesDialog(wx.Dialog):

	def __init__(self, parent,
	# Translators: The title of the audio themes manager dialog 
	title=_("Audio Themes Manager")):
		global NO_THEME_MSG
		super(ThemesDialog, self).__init__(parent, title=title)
		self.noThemeMessage = NO_THEME_MSG
		mainSizer = wx.BoxSizer(wx.VERTICAL)
		tasksSizer = wx.BoxSizer(wx.VERTICAL)
		# Translators: the text of the label of a listbox that shows all available themes
		tasksLabel = wx.StaticText(self, -1, label=_("Available Themes:"))
		tasksSizer.Add(tasksLabel)
		listID = wx.NewId()
		self.listBox = wx.ListBox(self, listID, style=wx.LB_SINGLE, size=(500, 250))
		tasksSizer.Add(self.listBox, proportion=8)
		mainSizer.Add(tasksSizer)
		buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
		useThemeID= wx.NewId()
		# Translators: The label of the button to set the selected theme as a user's active theme.
		useTheme = wx.Button(self, useThemeID, _("&Use Selected"))
		buttonsSizer.Add(useTheme)
		removeThemeID = wx.NewId()
		# Translators: The label of the buttons to remove the selected theme from installed themes list.
		removeTheme = wx.Button(self, removeThemeID, _("&Remove Selected"))
		buttonsSizer.Add(removeTheme)
		addThemeID = wx.NewId()
		# Translators: The label of the button to add a new theme to user's themes.
		addTheme = wx.Button(self, addThemeID, _("&Add New"))
		buttonsSizer.Add(addTheme)
		cancelButton = wx.Button(self, wx.ID_CANCEL, _("&Close"))
		buttonsSizer.Add(cancelButton)
		useTheme.SetDefault()
		mainSizer.Add(buttonsSizer)
		self.SetSizer(mainSizer)
		mainSizer.Fit(self)
		self.Bind( wx.EVT_BUTTON, self.onUseClick, id=useThemeID)
		self.Bind( wx.EVT_BUTTON, self.onRemoveClick, id=removeThemeID)
		self.Bind( wx.EVT_BUTTON, self.onAddClick, id=addThemeID)
		self.refresh()

	def onUseClick(self, event):
		if (self.listBox.GetSelection() == wx.NOT_FOUND) or (self.listBox.GetStringSelection == self.noThemeMessage):
			return
		utils.conf['using'] = self.listBox.GetStringSelection()
		utils.conf.write()
		self.askForRestart()

	def onRemoveClick(self, event):
		if (self.listBox.GetSelection() == wx.NOT_FOUND) or (self.listBox.GetStringSelection == self.noThemeMessage):
			return
		if msg(
		# Translators: message asking the user if he/she realy want to remove the selected theme.
		_("Do you want to remove the %s theme permanently?")%self.listBox.GetStringSelection(),
		# Translators: the title of the message box that ask the user if he/she realy want to remove the theme. 
		_("Are You Sure"),
		wx.YES_NO|wx.ICON_WARNING) == wx.NO:
			ui.message(_("Canceled"))
			return
		try:
			shutil.rmtree(os.path.join(themesdir(), self.listBox.GetStringSelection()))
			self.askForRestart()
			self.refresh()
		except:
			msg(
			# Translators: message telling the user that the deletion process was faild.
			_("Error Removing Theme"),
			# Translators: the title of the message telling that the deletion was faild
			_("Error"))

	def onAddClick(self, event):
		fd=wx.FileDialog(self,
		# Translators: The label of the add theme dialog to browse to audio theme file.
		message=_("ChooseTheme Package .ntm File"),
		# Translators: The name of  the file type of  audio themes files
		wildcard=(_("Theme Pack(*.{ext})")+"|*.{ext}").format(ext="ntm"),
		defaultDir="c:",style=wx.FD_OPEN)
		if fd.ShowModal()!=wx.ID_OK:
			return
		themeForIns=fd.GetPath()
		with zipfile.ZipFile(themeForIns, 'r') as z:
			for info in z.infolist():
				if isinstance(info.filename, str):
					info.filename = info.filename.decode("cp%d" % winKernel.kernel32.GetOEMCP())
				z.extract(info, themesdir())
		self.refresh()

	def askForRestart(self):
		m = msg(
		# Translators: message asking the user to restart his/her NVDA for changes to take effect.
		_("You need to restart this copy of NVDA for changes to take effect. do you want to restart now?"),
		# Translators: the title of the message asking for restart.
		_("Restart NVDA"),
		wx.YES_NO|wx.ICON_WARNING)
		if m == wx.YES:
			core.restart()

	def refresh(self):
		ins = []
		if os.path.exists(themesdir()):
			path = themesdir()
			ins = [itm for itm in os.listdir(path) if os.path.isdir(os.path.join(path, itm))]
		ins.append(self.noThemeMessage)
		self.listBox.Set(ins)

class ActionDialog(wx.Dialog):

	def __init__(self, parent,
	# Translators: the title of the themes editor dialog
	title=_("Audio Themes Editor")):
		super(ActionDialog, self).__init__(parent, title=title)
		mainSizer = wx.BoxSizer(wx.VERTICAL)
		tasksSizer = wx.BoxSizer(wx.VERTICAL)
		# Translators: the label asking the user to select an action to perform 
		tasksLabel = wx.StaticText(self, -1, label=_("What you would like to do?"))
		tasksSizer.Add(tasksLabel)
		mainSizer.Add(tasksSizer)
		buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
		CreateID = wx.NewId()
		# Translators: the label of a button to start the audio theme creation process.
		CreateTheme = wx.Button(self, CreateID, _("&Create New Audio Theme"))
		buttonsSizer.Add(CreateTheme)
		EditID = wx.NewId()
		# Translators: the label of a button that starts the editing process of the currently active theme.
		EditTheme = wx.Button(self, EditID, _("&Edit Current Theme"))
		buttonsSizer.Add(EditTheme)
		# Translators: the label of a button to Close this dialog
		cancelButton = wx.Button(self, wx.ID_CANCEL, _("&Close"))
		buttonsSizer.Add(cancelButton)
		CreateTheme.SetDefault()
		mainSizer.Add(buttonsSizer)
		self.SetSizer(mainSizer)
		mainSizer.Fit(self)
		self.Bind( wx.EVT_BUTTON, self.onCreateClick, id=CreateID)
		self.Bind( wx.EVT_BUTTON, self.onEditClick, id=EditID)

	def onCreateClick(self, evt):
		activate(CreateNew)
		#self.Destroy()

	def onEditClick(self, evt):
		activate(EditCurrent)
		#self.Destroy()


class EditCurrent(wx.Dialog):

	def __init__(self, parent,
	#Translators: the title of a dialog to edit the currently active theme.
	title=_("Edit Current Audio Theme")):
		super(EditCurrent, self).__init__(parent, title=title)
		self.sndMap = controlTypes.roleLabels.copy()
		self.extraMap = {
		#Translators: The last item entry in object roles combobox.
		"last": _("Last Item"),
		#Translators: the first item entry in object roles combobox 
		"first": _("First Item"),
		#Translators: the entry of help balloon in object roles combobox 
		"balloon":_("Help Balloon"),
		#Translators: web page loaded entry in object roles combobox
		"loaded":_("Web Page Loaded"),
		#Translators: protected fields entry in object roles combobox
		"protected":_("Password Edit Fields")}
		mainSizer = wx.BoxSizer(wx.VERTICAL)
		FileSizer = wx.BoxSizer(wx.VERTICAL)
		OptionsSizer = wx.BoxSizer(wx.VERTICAL)
		ItemSizer = wx.BoxSizer(wx.VERTICAL)
		ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
		# Translators: the label of the textbox to type the file path to include in the current theme.
		FileSizer.Add(wx.StaticText(self,-1,label=_("&Audio File Path")))
		self.FilePathTxtCtrl=wx.TextCtrl(self,wx.NewId())
		FileSizer.Add(self.FilePathTxtCtrl)
		BrowseID = wx.NewId()
		# Translators: the label of a button to browse the file system.
		BrowseButton = wx.Button(self, BrowseID, _("&Browse..."))
		FileSizer.Add(BrowseButton)
		# Translators: the label of the combo box contains all roles and the user must select one of them to assign the audio file to it.
		OptionsSizer.Add(wx.StaticText(self,-1,label=_("&Assign this audio to the following object:          ")))
		self.OptionsList = wx.Choice(self, wx.NewId(),choices=self.sndMap.values())
		CheckboxID = wx.NewId()
		# Translators: the label of a checkbox that controls wether to show special patterns that is not included in the nvda roles combobox.
		self.EditCheckbox=wx.CheckBox(self,CheckboxID,label=_("&Show Special patterns"))
		OptionsSizer.Add(self.OptionsList)
		OptionsSizer.Add(self.EditCheckbox)
		# Translators: label of the special patterns combobox
		ItemSizer.Add(wx.StaticText(self,-1,label=_("&select one of these special patterns:                    ")))
		self.ItemList=wx.Choice(self,wx.NewId(),choices=self.extraMap.values())
		ItemSizer.Add(self.ItemList)
		CheckID = wx.NewId()
		# Translators: the label of a button to speak a summary of the task to perform.
		self.CheckButton = wx.Button(self, CheckID, _("&Summary"))
		ItemSizer.Add(self.CheckButton)
		ApplyID = wx.NewId()
		# Translators: the label of a button to apply all changes and close the dialog
		ApplyButton = wx.Button(self, ApplyID, _("&Apply Changes"))
		ButtonSizer.Add(ApplyButton)
		cancelButton = wx.Button(self, wx.ID_CANCEL, _("&Close"))
		ButtonSizer.Add(cancelButton)
		mainSizer.Add(FileSizer)
		mainSizer.Add(OptionsSizer)
		mainSizer.Add(ItemSizer)
		mainSizer.Add(ButtonSizer)
		self.SetSizer(mainSizer)
		mainSizer.Fit(self)
		self.Bind( wx.EVT_BUTTON, self.onBrowseClick, id=BrowseID)
		self.Bind( wx.EVT_BUTTON, self.onCheckClick, id=CheckID)
		self.Bind( wx.EVT_BUTTON, self.onApplyClick, id=ApplyID)
		self.Bind(wx.EVT_CHECKBOX, self.EditChange, id=CheckboxID)
		self.ItemList.Disable()
		self.OptionsList.SetSelection(0)
		self.ItemList.SetSelection(0)
		self.EditCheckbox.SetValue(0)

	def onBrowseClick(self, evt):
		fd=wx.FileDialog(self,
		# Translators: the title of dialog that allows the user to browse for audio files.
		message=_("Choos Audio File"),
		# Translators: the file type of the file to be picked up.
		wildcard=(_("Wave Files(*.{ext})")+"|*.{ext}").format(ext="wav"),
		style=wx.FD_OPEN)
		if fd.ShowModal()!=wx.ID_OK:
			return
		AudioFilePath=fd.GetPath()
		try:
			__ = Sound3D(AudioFilePath)
		except UnsupportedChannelCountError, InternalFileError:
			ui.message(_("Can not load this file, Please select another one!"))
		self.FilePathTxtCtrl.SetValue(AudioFilePath)

	def EditChange(self, evt):
		val = self.EditCheckbox.GetValue()
		if val:
			self.OptionsList.Disable()
			self.ItemList.Enable()
		else:
			self.OptionsList.Enable()
			self.ItemList.Disable()

	def onCheckClick(self, evt, silence=False):
		if self.FilePathTxtCtrl.IsEmpty():
			# Translators: message warning the user to select an audio file before attempting to perform any action.
			ui.message(_("Please Select An Audio File First!"))
			self.FilePathTxtCtrl.SetFocus()
			return
		if  os.path.exists(self.FilePathTxtCtrl.GetValue()) == False:
			# Translators: message warning the user that the file he/she entered does not exist
			ui.message("The File you entered does not exists")
			self.FilePathTxtCtrl.Clear()
			return
		if not silence:
			res = self.OptionsList.GetStringSelection() if self.OptionsList.IsEnabled() else self.ItemList.GetStringSelection()
			ui.message(
			# Translators: message Showing a summary of the task that will be performed.
			_("The audio file {filename} will be assigned to the {objrole}").format(filename=os.path.split(self.FilePathTxtCtrl.GetValue())[1], objrole=res))
		return True

	def onApplyClick(self, evt):
		if self.onCheckClick(evt, silence=True) == None:
			self.FilePathTxtCtrl.SetFocus()
			return
		selection = self.OptionsList.GetStringSelection() if self.OptionsList.IsEnabled() else self.ItemList.GetStringSelection()
		snd = self.sndMap.copy()
		snd.update(self.extraMap)
		for k,v in snd.iteritems():
			if v == selection:
				self.apply(k)

	def apply(self, val):
		destFile = os.path.join(currentThemePath(), "%s.wav" %val)
		msg = ""
		if os.path.exists(destFile):
			# Translators: message telling the user that the file was replaced.
			msg=_("File Replaced")
		else:
			# Translators: message telling the user that the file was added.
			msg=_("File added")
		try:
			shutil.copy(self.FilePathTxtCtrl.GetValue(), destFile)
			# Translators: message telling the user that the action was performed successfully
			ui.message("%s Successfuly" %msg)
			self.FilePathTxtCtrl.Clear()
		except IOError, Error:
			# Translators: message telling the user that the action was faild.
			ui.message(_("Oparation Faild"))

class CreateNew(wx.Dialog):

	def __init__(self, parent, 
	# Translators: the title of create new audio theme dialog.
	title=_("Create New Audio Theme")):
		super(CreateNew, self).__init__(parent, title=title)
		d = wx.TextEntryDialog(self, 
		# Translators: the label of the edit field to enter a name for a new theme package.
		_("Enter a name for your theme package:"), 
		# Translators: the title of a dialog asking the user to enter a name for a new theme pack.
		_("enter a name"))
		self.themeName = "Default"
		if d.ShowModal() == wx.ID_OK:
			self.themeName = d.GetValue()
		else:
			self.Destroy()
			return
		if not utils.LOADED:
			utils.initialize_camlorn_audio()
		mainSizer = wx.BoxSizer(wx.VERTICAL)
		BasePathSizer = wx.BoxSizer(wx.VERTICAL)
		ListSizer = wx.BoxSizer(wx.VERTICAL)
		ComboSizer = wx.BoxSizer(wx.VERTICAL)
		ButtonsSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.directory=""
		self.done = True
		self.sndMap = controlTypes.roleLabels.copy()
		extraMap = {
		#Translators: The last item entry in object roles combobox.
		"last": _("Last Item"),
		#Translators: the first item entry in object roles combobox 
		"first": _("First Item"),
		#Translators: the entry of help balloon in object roles combobox 
		"balloon":_("Help Balloon"),
		#Translators: web page loaded entry in object roles combobox
		"loaded":_("Web Page Loaded"),
		#Translators: protected fields entry in object roles combobox
		"protected":_("Password Edit Fields")}
		self.sndMap.update(extraMap)
		self.BasePathTxt = wx.TextCtrl(self, wx.ID_ANY)
		PathID = wx.NewId()
		# Translators: the label of a button that allows the user to select a directory to act as his/her base path.
		self.PathButton = wx.Button(self, PathID, _("&Select Base Path"))
		# Translators: the label of the base path edit feild
		BasePathSizer.Add(wx.StaticText(self, -1, label=_("Base path:")))
		BasePathSizer.Add(self.BasePathTxt)
		BasePathSizer.Add(self.PathButton)
		# Translators: the label of a listbox contains all audio files found in a base path
		ListSizer.Add(wx.StaticText(self, -1, label=_("Audio files found in this base path:")))
		listID = wx.NewId()
		self.listBox = wx.ListBox(self, listID, style=wx.LB_SINGLE, size=(500, 250))
		ListSizer.Add(self.listBox)
		# Translators: the label of the combobox contains all nvda object roles and the user must select one of them to assign the audio file to it.
		ComboSizer.Add(wx.StaticText(self, -1, label=_("Assign Selected Audio to this object:")))
		self.RolesList = wx.Choice(self, wx.NewId(),choices=self.sndMap.values())
		ComboSizer.Add(self.RolesList)
		AssignID = wx.NewId()
		# Translators: the label of a button to assign selected audio file to the selected role.
		AssignButton = wx.Button(self, AssignID, _("&Assign"))
		ComboSizer.Add(AssignButton)
		PackID= wx.NewId()
		# Translators: the label of a button to package the audio theme
		PackButton = wx.Button(self, PackID, _("&Package Audio Theme"))
		cancelButton = wx.Button(self, wx.ID_CANCEL, _("&Close"))
		ButtonsSizer.Add(PackButton)
		ButtonsSizer.Add(cancelButton)
		mainSizer.Add(BasePathSizer)
		mainSizer.Add(ListSizer)
		mainSizer.Add(ComboSizer)
		mainSizer.Add(ButtonsSizer)
		self.SetSizer(mainSizer)
		mainSizer.Fit(self)
		self.Bind( wx.EVT_BUTTON, self.onPathClick, id=PathID)
		self.Bind( wx.EVT_BUTTON, self.onAssignClick, id=AssignID)
		self.Bind( wx.EVT_BUTTON, self.onPackClick, id=PackID)
		self.Bind( wx.EVT_BUTTON, self.onClose, id=wx.ID_CANCEL)
		self.BasePathTxt.Disable()
		self.RolesList.SetSelection(0)
		self.Bind(wx.EVT_LISTBOX, self.OnSelect, id=listID)
		temp = tempfile.mkdtemp()
		self.tempDir = os.path.join(temp, self.themeName)
		os.mkdir(self.tempDir)

	def listFiles(self):
		for entry in self.validFiles():
			self.listBox.Append(entry)

	def validFiles(self):
		files = [os.path.join(self.directory, filename) for filename in os.listdir(self.directory)]
		for f in files:
			try:
				__ = Sound3D(f)
			except UnsupportedChannelCountError:
				files.remove(f)
			except InternalFileError:
				gui.messageBox(
				# Translators: message telling the user that there is no valid sound in the selected folder.
				_("Could not load any sound file from %{audioPath}. the possible causes for this are:\n- You do not have rights to access this folder.\n Please move this folder to your documents path.").format(audioPath=self.directory),
				# Translators: the title of the message telling that the sounds loading process was faild
				_("Error Loading Files"), wx.ICON_ERROR)
				self.PathButton.Enable()
				return list()
		return [os.path.split(path)[-1] for path in files]

	def onPathClick(self, evt):
		path=""
		dialog = wx.DirDialog(self, 
		# Translators: the title of a dialog asking user to select the folder that contains audio files.
		message=_("Select the folder that contains audio files"), style=wx.DD_DIR_MUST_EXIST)
		if dialog.ShowModal() == wx.ID_OK:
			path = dialog.GetPath()
		self.directory = path
		self.BasePathTxt.SetValue(path)
		self.BasePathTxt.Enable()
		self.PathButton.Disable()
		self.listFiles()
		self.listBox.SetFocus()

	def onAssignClick(self, evt):
		fullPath = os.path.join(self.directory, self.listBox.GetStringSelection())
		for k, v in self.sndMap.iteritems():
			if v == self.RolesList.GetStringSelection():
				try:
					shutil.copy(fullPath, os.path.join(self.tempDir, "%s.wav" %k))
					# Translators: message telling the user that the assigning process was successfull
					ui.message(_("File Assigned Successfuly"))
					self.RolesList.Delete(self.RolesList.GetSelection())
					self.RolesList.SetSelection(0)
					self.listBox.SetFocus()
					self.done=False
				except:
					# Translators: message telling the user that the assigning process was faild
					ui.message(_("Faild"))

	def onPackClick(self, evt):
		self.done = True
		makeZipFile(os.path.join(self.directory, "%s.ntm" %self.themeName), self.tempDir)
		shutil.rmtree(self.tempDir)
		wx.MessageDialog(None, 
		# Translators: message informing the user that the theme was created and give him/her the new theme's location on the disk.
		_("Theme Package has been created, you can find it in %s") %os.path.join(self.directory, "%s.ntm"%self.themeName), 
		# Translators: message telling the user that the process was completed.
		_("Done"), wx.OK | wx.ICON_INFORMATION).ShowModal()
		self.Destroy()

	def onClose(self, evt):
		if self.done == True:
			self.Destroy()
			return
		dial = wx.MessageDialog(None, 
		# Translators: message asking the user if he/she want to quit without saving changes.
		_("Changes not saved, do you want to quit?"), 
		# Translators: the title of the message asking the user if he/she want to quit.
		_("Are you sure?"), wx.YES_NO | wx.ICON_QUESTION)
		if dial.ShowModal() == wx.ID_NO:
			return
		try:
			os.remove(self.tempDir)
		except: pass
		self.Destroy()

	def OnSelect(self, evt):
		play(os.path.join(self.directory, self.listBox.GetStringSelection()))

def makeZipFile(output_filename, source_dir):
	relroot = os.path.abspath(os.path.join(source_dir, os.pardir))
	with zipfile.ZipFile(output_filename, "w", zipfile.ZIP_DEFLATED) as zip:
		for root, dirs, files in os.walk(source_dir):
			# add directory (needed for empty dirs)
			zip.write(root, os.path.relpath(root, relroot))
			for file in files:
				filename = os.path.join(root, file)
				if os.path.isfile(filename): # regular files only
					arcname = os.path.join(os.path.relpath(root, relroot), file)
					zip.write(filename, arcname)

def activate(dg):
	gui.mainFrame._popupSettingsDialog(dg)
