# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 'AudioThemes' '4.0'\n"
"Report-Msgid-Bugs-To: 'nvda-translations@freelists.org'\n"
"POT-Creation-Date: 2014-08-01 21:37+0300\n"
"PO-Revision-Date: 2014-08-01 21:43+0200\n"
"Last-Translator: \n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"

#. Translators: the label of the menu item to open the audio themes manager dialog.
#: addon\globalPlugins\AudioThemes\__init__.py:35
msgid "Audio Themes &Manager..."
msgstr "مديرُ الحِزَمِ الصَّوْتِيَّة"

#. Translators: the tooltip text of the menu item that opens audio themes manager dialog
#: addon\globalPlugins\AudioThemes\__init__.py:37
msgid "Manage themes"
msgstr "إدارةُ الحِزَمِ الصوتية"

#. Translators: The label of the menu item that opens the audio themes editor dialog
#: addon\globalPlugins\AudioThemes\__init__.py:42
msgid "Audio Themes &Editor..."
msgstr "مُحَرِّرُ الحِزَمِ الصَّوْتِيَّة "

#. Translators: The tooltip of the menu item that opens the audio themes editor dialog
#: addon\globalPlugins\AudioThemes\__init__.py:44
msgid "Create and/or edit the current theme"
msgstr "إنشاءُ أو تحريرُ الحِزَمِ الصَّوْتِيَّة"

#. Translators: The label of the menu item that opens the help document for this add-on
#: addon\globalPlugins\AudioThemes\__init__.py:47
msgid "&Help"
msgstr "المُسَاعَدَة"

#. Translators: The tooltip text of the help menu item
#: addon\globalPlugins\AudioThemes\__init__.py:49
msgid "Show Help Content"
msgstr "عَرْضُ مَلَفِّ المُسَاعَدَةِ "

#. Translators: The label for this add-on's  menu
#: addon\globalPlugins\AudioThemes\__init__.py:55
msgid "&Audio Themes"
msgstr "الحِزَمِ الصَّوْتِيَّة"

#: addon\globalPlugins\AudioThemes\interface.py:25
msgid "Turn Off Audio Themes"
msgstr "إيقافُ الحِّزَمِ الصوتية"

#. Translators: The title of the audio themes manager dialog
#: addon\globalPlugins\AudioThemes\interface.py:31
msgid "Audio Themes Manager"
msgstr "مُديرُ الحِزَمِ الصَّوتيَّة"

#. Translators: the text of the label of a listbox that shows all available themes
#: addon\globalPlugins\AudioThemes\interface.py:38
msgid "Available Themes:"
msgstr "الحِزَمُ المُثَبَّتَة "

#. Translators: The label of the button to set the selected theme as a user's active theme.
#: addon\globalPlugins\AudioThemes\interface.py:47
msgid "&Use Selected"
msgstr "إستخدامُ المُحَدَّدْ"

#. Translators: The label of the buttons to remove the selected theme from installed themes list.
#: addon\globalPlugins\AudioThemes\interface.py:51
msgid "&Remove Selected"
msgstr "حَذْفُ المُحَدَّد"

#. Translators: The label of the button to add a new theme to user's themes.
#: addon\globalPlugins\AudioThemes\interface.py:55
msgid "&Add New"
msgstr "إضافةُ حِزْمَةٍ صوتيةٍ جديدة"

#. Translators: the label of a button to Close this dialog
#: addon\globalPlugins\AudioThemes\interface.py:57
#: addon\globalPlugins\AudioThemes\interface.py:154
#: addon\globalPlugins\AudioThemes\interface.py:223
#: addon\globalPlugins\AudioThemes\interface.py:375
msgid "&Close"
msgstr "إغلاق"

#. Translators: message asking the user if he/she realy want to remove the selected theme.
#: addon\globalPlugins\AudioThemes\interface.py:80
#, python-format
msgid "Do you want to remove the %s theme permanently?"
msgstr "هَلْ تُرِيدُ حَذْفَ الحِزْمة %s بشكلٍ نهائي."

#. Translators: the title of the message box that ask the user if he/she realy want to remove the theme.
#: addon\globalPlugins\AudioThemes\interface.py:82
msgid "Are You Sure"
msgstr "هل أنتَ مُتَأكِّدْ"

#: addon\globalPlugins\AudioThemes\interface.py:84
msgid "Canceled"
msgstr "تَمَّ إلغاءُ العملية"

#. Translators: message telling the user that the deletion process was faild.
#: addon\globalPlugins\AudioThemes\interface.py:93
msgid "Error Removing Theme"
msgstr "حَدَثَ خطأٌ أثناءَ حَذْفِ ملفاتِ الحِزْمةِ الصوتية"

#. Translators: the title of the message telling that the deletion was faild
#: addon\globalPlugins\AudioThemes\interface.py:95
msgid "Error"
msgstr "خطأ"

#. Translators: The label of the add theme dialog to browse to audio theme file.
#: addon\globalPlugins\AudioThemes\interface.py:100
msgid "ChooseTheme Package .ntm File"
msgstr "إختر الملف الذي يضم الحزمة الصوتية والذي يكون بإمتداد NTM"

#. Translators: The name of  the file type of  audio themes files
#: addon\globalPlugins\AudioThemes\interface.py:102
msgid "Theme Pack(*.{ext})"
msgstr "الحِزَمُ الصوتيةُ"

#. Translators: message asking the user to restart his/her NVDA for changes to take effect.
#: addon\globalPlugins\AudioThemes\interface.py:117
msgid ""
"You need to restart this copy of NVDA for changes to take effect. do you "
"want to restart now?"
msgstr ""
"تحتاجُ إلى إعادةِ تشغيلِ NVDA حتى تُصْبِحَ التَّغْيِراتُ فَعَّالة. هل ترغبُ في إعادةِ التشغيلِ "
"الآن؟"

#. Translators: the title of the message asking for restart.
#: addon\globalPlugins\AudioThemes\interface.py:119
msgid "Restart NVDA"
msgstr "يَتَطَلَّبُ الأمرُ إعادةَ التشغيلْ"

#. Translators: the title of the themes editor dialog
#: addon\globalPlugins\AudioThemes\interface.py:136
msgid "Audio Themes Editor"
msgstr "مُحَرِّرُ الحِزَمِ الصوتية"

#. Translators: the label asking the user to select an action to perform
#: addon\globalPlugins\AudioThemes\interface.py:141
msgid "What you would like to do?"
msgstr "ماذا تُرِيدُ أنْ تَفْعَلْ؟"

#. Translators: the label of a button to start the audio theme creation process.
#: addon\globalPlugins\AudioThemes\interface.py:147
msgid "&Create New Audio Theme"
msgstr "إنشاءُ حِزْمَةٍ صوتيةٍ جديدةْ"

#. Translators: the label of a button that starts the editing process of the currently active theme.
#: addon\globalPlugins\AudioThemes\interface.py:151
msgid "&Edit Current Theme"
msgstr "تحريرُ الحِزْمَةِ الصوتيةِ الحالية"

#. Translators: the title of a dialog to edit the currently active theme.
#: addon\globalPlugins\AudioThemes\interface.py:176
msgid "Edit Current Audio Theme"
msgstr "تحريرُ الحِزْمَةِ الصوتيةِ الحالية"

#. Translators: The last item entry in object roles combobox.
#: addon\globalPlugins\AudioThemes\interface.py:181
#: addon\globalPlugins\AudioThemes\interface.py:341
msgid "Last Item"
msgstr "العُنْصُرْ الأخير"

#. Translators: the first item entry in object roles combobox
#: addon\globalPlugins\AudioThemes\interface.py:183
#: addon\globalPlugins\AudioThemes\interface.py:343
msgid "First Item"
msgstr "العُنْصُر الأول"

#. Translators: the entry of help balloon in object roles combobox
#: addon\globalPlugins\AudioThemes\interface.py:185
#: addon\globalPlugins\AudioThemes\interface.py:345
msgid "Help Balloon"
msgstr "بَالُونَات المُساعدَة"

#. Translators: web page loaded entry in object roles combobox
#: addon\globalPlugins\AudioThemes\interface.py:187
#: addon\globalPlugins\AudioThemes\interface.py:347
msgid "Web Page Loaded"
msgstr "إنتهاءِ تحميلِ صفحةْ الويب"

#. Translators: protected fields entry in object roles combobox
#: addon\globalPlugins\AudioThemes\interface.py:189
#: addon\globalPlugins\AudioThemes\interface.py:349
msgid "Password Edit Fields"
msgstr "حقلُ إدخالِ كلمةِ المرور"

#. Translators: the label of the textbox to type the file path to include in the current theme.
#: addon\globalPlugins\AudioThemes\interface.py:196
msgid "&Audio File Path"
msgstr "مَسارُ المَلَفِّ الصوتيِّ "

#. Translators: the label of a button to browse the file system.
#: addon\globalPlugins\AudioThemes\interface.py:201
msgid "&Browse..."
msgstr "تَصَفُّحْ"

#. Translators: the label of the combo box contains all roles and the user must select one of them to assign the audio file to it.
#: addon\globalPlugins\AudioThemes\interface.py:204
msgid "&Assign this audio to the following object:          "
msgstr "حَدِّدْ نوعَ الكائِنِ الذي سيتمُّ تخصيصُ الملفِّ له:"

#. Translators: the label of a checkbox that controls wether to show special patterns that is not included in the nvda roles combobox.
#: addon\globalPlugins\AudioThemes\interface.py:208
msgid "&Show Special patterns"
msgstr "إظهارُ الأنماطِ الخاصَّةِ"

#. Translators: label of the special patterns combobox
#: addon\globalPlugins\AudioThemes\interface.py:212
msgid "&select one of these special patterns:                    "
msgstr "إخترْ أحدَ هذه الأنماطِ الخاصة:"

#. Translators: the label of a button to speak a summary of the task to perform.
#: addon\globalPlugins\AudioThemes\interface.py:217
msgid "&Summary"
msgstr "مُلَخَّصْ"

#. Translators: the label of a button to apply all changes and close the dialog
#: addon\globalPlugins\AudioThemes\interface.py:221
msgid "&Apply Changes"
msgstr "تَنْفيذُ التَّغْيراتْ"

#. Translators: the title of dialog that allows the user to browse for audio files.
#: addon\globalPlugins\AudioThemes\interface.py:243
msgid "Choos Audio File"
msgstr "إختر ملفَّاً صوتياً"

#. Translators: the file type of the file to be picked up.
#: addon\globalPlugins\AudioThemes\interface.py:245
msgid "Wave Files(*.{ext})"
msgstr "ملفات الصوت Wave"

#: addon\globalPlugins\AudioThemes\interface.py:253
msgid "Can not load this file, Please select another one!"
msgstr "لايمكنُ تحميلُ هذا الملفْ. رَجاءً إختر ملفاً آخر."

#. Translators: message warning the user to select an audio file before attempting to perform any action.
#: addon\globalPlugins\AudioThemes\interface.py:268
msgid "Please Select An Audio File First!"
msgstr "رَجاءً قُمْ بإختيارِ أحدِ الملفاتِ الصوتية"

#. Translators: message Showing a summary of the task that will be performed.
#: addon\globalPlugins\AudioThemes\interface.py:280
msgid "The audio file {filename} will be assigned to the {objrole}"
msgstr "الملف {filename} سَيَتِّمُ تخصيصهُ للكائنات مِنْ نوعِ {objrole}"

#. Translators: message telling the user that the file was replaced.
#: addon\globalPlugins\AudioThemes\interface.py:299
msgid "File Replaced"
msgstr "تَمَّ إستبدالُ المَلَفِّ بنجاحْ"

#. Translators: message telling the user that the file was added.
#: addon\globalPlugins\AudioThemes\interface.py:302
msgid "File added"
msgstr "تَمَّتْ إضافةُ المَلَفِّ بنجاحْ"

#. Translators: message telling the user that the action was faild.
#: addon\globalPlugins\AudioThemes\interface.py:310
msgid "Oparation Faild"
msgstr "فشلتْ العملية"

#. Translators: the title of create new audio theme dialog.
#: addon\globalPlugins\AudioThemes\interface.py:316
msgid "Create New Audio Theme"
msgstr "إنشاءُ حِزْمَةٍ صوتيةٍ جديدة"

#. Translators: the label of the edit field to enter a name for a new theme package.
#: addon\globalPlugins\AudioThemes\interface.py:320
msgid "Enter a name for your theme package:"
msgstr "إختر إسماً لحِزْمَتِكَ الصوتية الجديدة:"

#. Translators: the title of a dialog asking the user to enter a name for a new theme pack.
#: addon\globalPlugins\AudioThemes\interface.py:322
msgid "enter a name"
msgstr "أَدْخِلْ إسماً"

#. Translators: the label of a button that allows the user to select a directory to act as his/her base path.
#: addon\globalPlugins\AudioThemes\interface.py:354
msgid "&Select Base Path"
msgstr "إختر مساراً للعمل"

#. Translators: the label of the base path edit feild
#: addon\globalPlugins\AudioThemes\interface.py:356
msgid "Base path:"
msgstr "المسارُ الأساسيْ"

#. Translators: the label of a listbox contains all audio files found in a base path
#: addon\globalPlugins\AudioThemes\interface.py:360
msgid "Audio files found in this base path:"
msgstr "الملفاتُ الصوتيةُ التي تمَّ العثورُ عليها"

#. Translators: the label of the combobox contains all nvda object roles and the user must select one of them to assign the audio file to it.
#: addon\globalPlugins\AudioThemes\interface.py:365
msgid "Assign Selected Audio to this object:"
msgstr "حَدِّدْ نوعَ الكائنِ الذي سيتمُّ تخصيصُ الملفِّ لهُ."

#. Translators: the label of a button to assign selected audio file to the selected role.
#: addon\globalPlugins\AudioThemes\interface.py:370
msgid "&Assign"
msgstr "تخصيص"

#. Translators: the label of a button to package the audio theme
#: addon\globalPlugins\AudioThemes\interface.py:374
msgid "&Package Audio Theme"
msgstr "تَجْهيزُ الحِزْمَةِ الصوتية"

#. Translators: message telling the user that there is no valid sound in the selected folder.
#: addon\globalPlugins\AudioThemes\interface.py:409
msgid ""
"Could not load any sound file from %{audioPath}. the possible causes for "
"this are:\n"
"- You do not have rights to access this folder.\n"
" Please move this folder to your documents path."
msgstr ""
"تَعَذَّرَ تحميلُ الملفاتِ من المسارِ %{audioPath}. قد يكون هذا ناتجاً عن عدم حصولك "
"على الصلاحيات اللازمة لقراءة هذا المسار. رَجاءً قم بنقل هذا المُجَلَّدْ إلى مسار "
"المستندات الخاص بك."

#. Translators: the title of the message telling that the sounds loading process was faild
#: addon\globalPlugins\AudioThemes\interface.py:411
msgid "Error Loading Files"
msgstr "حَدَثَ خطأٌ أثناءَ تحميلِ الملفات"

#. Translators: the title of a dialog asking user to select the folder that contains audio files.
#: addon\globalPlugins\AudioThemes\interface.py:420
msgid "Select the folder that contains audio files"
msgstr "إختر المُجَلَّدْ الذي يتضمنُ ملفاتَ الصوتِ"

#. Translators: message telling the user that the assigning process was successfull
#: addon\globalPlugins\AudioThemes\interface.py:437
msgid "File Assigned Successfuly"
msgstr "تَمَّ تخصيصُ المَلَفِّ بنجاحْ"

#. Translators: message telling the user that the assigning process was faild
#: addon\globalPlugins\AudioThemes\interface.py:444
msgid "Faild"
msgstr "فشلت العملية"

#. Translators: message informing the user that the theme was created and give him/her the new theme's location on the disk.
#: addon\globalPlugins\AudioThemes\interface.py:452
#, python-format
msgid "Theme Package has been created, you can find it in %s"
msgstr "تَمَّ إنشاءُ الحِزْمَةِ الصوتيةِ، يمكنكَ أن تجدها في المسار %s"

#. Translators: message telling the user that the process was completed.
#: addon\globalPlugins\AudioThemes\interface.py:454
msgid "Done"
msgstr "تَمْ"

#. Translators: message asking the user if he/she want to quit without saving changes.
#: addon\globalPlugins\AudioThemes\interface.py:463
msgid "Changes not saved, do you want to quit?"
msgstr "لم يتم حفظُ التَّغْيرات، هل تريدُ الخروج؟"

#. Translators: the title of the message asking the user if he/she want to quit.
#: addon\globalPlugins\AudioThemes\interface.py:465
msgid "Are you sure?"
msgstr "هل أنت متأكِّد؟"

#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on to be shown on installation and add-on information.
#: buildVars.py:17
msgid "Audio Themes 3D"
msgstr "الحِزَمُ الصوتيةُ بأصواتٍ ثُلاثِيَّةُ الأبعادْ"

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
#: buildVars.py:20
msgid ""
"This Add-on will inritch your navigation by adding a little twist,\n"
"\tthat is Audio themes. When ever an specific object got nvda focus a "
"special tone will be played.\n"
"\tyou can add and remove Audio themes. or even create and share your own "
"themes whith a personal tuch.\n"
"\tfor more info open the help file from the add-on menu"
msgstr ""
"هذه الإضافةُ سَتُضِيفُ حيلةً صغيرةً لـ NVDA تتمثلُ في الحِزَمِ الصوتيةِ حيثُ سيتمُّ تخصيصُ "
"صوتٍ معينٍ لكلِّ كائنٍ مِن الكائناتِ مثلَ الأزرارِ و مربعاتَ التحريرِ علاوةً على ذلك  "
"يمكنكَ إنشاءُ ومشاركةُ الحِزَمُ الصوتيةِ الخاصةُ بكَ مع بقيةِ المستخدمين. لمزيدٍ مِن "
"المعلوماتِ قم بفتحِ ملفِّ  المساعدةِ مِن قائمةِ الإضافة"

#~ msgid "play or stop loop music of the currently active theme"
#~ msgstr "يَقُومُ بِتشغيلِ أو إيقافِ الموسيقى الخاصة بالحزمةِ الصوتيةِ الحالية"
